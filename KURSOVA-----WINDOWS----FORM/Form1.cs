﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace KURSOVA_____WINDOWS____FORM
{

    public partial class Form1 : Form
    {

        Info dataBase = new Info();

        

        public Form1()
        {
            InitializeComponent();
        }


        private void CreateColon()
        {
            dataGridView1.Columns.Add("number","Номер");
            dataGridView1.Columns.Add("names_tournament","Назва турніру");
            dataGridView1.Columns.Add("team_count_tournaments", "Команда з найбільшою кількістю титулів турніру");
            dataGridView1.Columns.Add("years_tournament", "Рік заснування турніру");

        }




        private void ReadST(DataGridView dgw, IDataRecord record)  //IData... dostup do znachennia stovptsiv dlia kozhnoyi stroki
        {
            dgw.Rows.Add(record.GetInt32(0),record.GetString(1),record.GetString(2),record.GetInt32(3));


        }


        private void printInfo(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string vibString = $"select * from information_db";
                SqlCommand command = new SqlCommand(vibString,dataBase.GetSqlConnection());
            dataBase.Opendb();
            SqlDataReader reader = command.ExecuteReader();
            
            while (reader.Read())
            {

                ReadST(dgw, reader);
            }

            reader.Close();
        }





        private void Form1_Load(object sender, EventArgs e)
        {


            CreateColon();
            printInfo(dataGridView1);

        }



        private void search(DataGridView dgw)
        {
            dgw.Rows.Clear();

string searchStrok = $"select * from information_db where concat (number, names_tournament, team_count_tournaments,years_tournament) like '%"+textBox_search.Text+"%'";

            SqlCommand search = new SqlCommand(searchStrok,dataBase.GetSqlConnection());

            dataBase.Opendb();
            SqlDataReader read = search.ExecuteReader();
            while (read.Read())
            {
                ReadST(dgw,read);

            }
            read.Close();

        }



        private void textBox_search_TextChanged(object sender, EventArgs e)
        {

            search(dataGridView1);


        }
    }
}
